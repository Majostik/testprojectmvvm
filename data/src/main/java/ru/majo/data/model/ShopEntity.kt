package ru.majo.data.model

import java.util.Date

data class ShopEntity(
    val id: Int,
    val number: Int,
    val name: String,
    val address: String,
    val yearEarnings: Int,
    val coordinates: CoordinatesEntity,
    val openDate: Date?,
    val status: String
)