package ru.majo.data.model

data class CoordinatesEntity(
    val latitude: Double,
    val longitude: Double
)