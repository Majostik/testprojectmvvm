package ru.majo.data.mapper

import ru.majo.data.model.CoordinatesEntity
import ru.majo.data.model.ShopEntity
import ru.majo.domain.entities.Coordinates
import ru.majo.domain.entities.Shop
import ru.majo.domain.entities.ShopStatus
import javax.inject.Inject

class ShopMapper @Inject constructor() : Mapper<ShopEntity, Shop>{
    override fun mapFromEntity(entity: ShopEntity): Shop =
        Shop(
            id = entity.id,
            number = entity.number,
            name = entity.name,
            address = entity.address,
            yearEarnings = entity.yearEarnings,
            coordinates = Coordinates(entity.coordinates.latitude, entity.coordinates.longitude),
            openDate = entity.openDate,
            status = Shop.convertStatus(entity.status)
        )

    override fun mapToEntity(model: Shop): ShopEntity =
        ShopEntity(
            id = model.id,
            number = model.number,
            name = model.name,
            address = model.address,
            yearEarnings = model.yearEarnings,
            coordinates = CoordinatesEntity(model.coordinates.latitude, model.coordinates.longitude),
            openDate = model.openDate,
            status = model.status.type
        )

}