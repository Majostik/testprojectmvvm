package ru.majo.data.mapper

interface Mapper<E, M> {

    fun mapFromEntity(entity: E): M

    fun mapToEntity(model: M): E

}