package ru.majo.data.repository

import io.reactivex.Single
import ru.majo.data.datasource.DataSource
import ru.majo.data.mapper.ShopMapper
import ru.majo.domain.entities.Shop
import ru.majo.domain.repository.ShopsRepository
import javax.inject.Inject

class ShopsRepositoryImpl @Inject constructor(
    private val shopMapper: ShopMapper,
    private val dataSource: DataSource
): ShopsRepository {

    override fun getShops(): Single<List<Shop>> {
        return dataSource.getShops().map { shops ->
            Thread.sleep(2000)
            shops.map {
                shopMapper.mapFromEntity(it)
            }
        }
    }

    override fun getShopById(id: Int): Single<Shop> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}