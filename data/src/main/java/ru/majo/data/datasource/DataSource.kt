package ru.majo.data.datasource

import io.reactivex.Single
import ru.majo.data.model.CoordinatesEntity
import ru.majo.data.model.ShopEntity
import java.util.*
import javax.inject.Inject

class DataSource  @Inject constructor(){

    fun getShops(): Single<List<ShopEntity>> {
        return Single.just(stubShops())
    }

    private fun stubShops(): List<ShopEntity> {
        val shops = mutableListOf<ShopEntity>()
        for (i in 1 until 30) {
            shops.add(createShop(i))
        }
        return shops
    }

    private fun createShop(index: Int): ShopEntity {
        return ShopEntity(index, index, "Shop #" + index, "Address " + index,
            index *10000, CoordinatesEntity(0.0,0.0 ), Date(), "closed")
    }
}