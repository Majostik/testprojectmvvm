package ru.majo.data.factory

import ru.majo.data.model.CoordinatesEntity
import ru.majo.data.model.ShopEntity
import ru.majo.domain.entities.Coordinates
import ru.majo.domain.entities.Shop
import ru.majo.domain.entities.ShopStatus
import java.util.Date
import java.util.concurrent.ThreadLocalRandom

class ShopFactory {
    companion object {

        fun createShop(): Shop {
            return Shop(randomInt(), randomInt(), "", "", randomInt(),
                Coordinates(randomDouble(), randomDouble()), Date(), ShopStatus.CLOSED)
        }

        fun createShopEntity(): ShopEntity {
            return ShopEntity(randomInt(), randomInt(), "", "", randomInt(),
                CoordinatesEntity(randomDouble(), randomDouble()), Date(), "closed")
        }

        fun createShopEntitiesList(): List<ShopEntity> {
            return mutableListOf<ShopEntity>().apply {
                add(createShopEntity())
                add(createShopEntity())
            }
        }

        fun createShopList(): List<Shop> {
            return mutableListOf<Shop>().apply {
                add(createShop())
                add(createShop())
            }
        }

        private fun randomInt(): Int {
            return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
        }

        private fun randomDouble(): Double {
            return ThreadLocalRandom.current().nextInt(0, 1000 + 1).toDouble()
        }
    }
}