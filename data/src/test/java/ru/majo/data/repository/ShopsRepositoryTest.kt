package ru.majo.data.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.majo.data.datasource.DataSource
import ru.majo.data.factory.ShopFactory
import ru.majo.data.mapper.ShopMapper
import ru.majo.data.model.ShopEntity
import ru.majo.domain.entities.Shop

@RunWith(MockitoJUnitRunner::class)
class ShopsRepositoryTest {

    private val shopMapper: ShopMapper = mock()
    private val dataSource: DataSource = mock()

    private val shopsRepositoryImpl = ShopsRepositoryImpl(shopMapper, dataSource)

    @Test
    fun getShopsComplete() {
        whenever(dataSource.getShops()).thenReturn(Single.just(emptyList()))

        shopsRepositoryImpl.getShops()
            .test()
            .assertComplete()

        verify(dataSource).getShops()
    }

    @Test
    fun getShopsResult() {
        val shopEntities = ShopFactory.createShopEntitiesList()
        val shops = ShopFactory.createShopList()

        whenever(dataSource.getShops()).thenReturn(Single.just(shopEntities))

        shopEntities.forEachIndexed { index, shopEntity ->
            whenever(shopMapper.mapFromEntity(shopEntity)).thenReturn(shops[index])
        }

        shopsRepositoryImpl.getShops()
            .test()
            .assertValue(shops)

        verify(dataSource).getShops()
        shopEntities.forEach {
            verify(shopMapper).mapFromEntity(it)
        }
    }

    @Test
    fun getShopsError() {
        val response = Throwable("Error response")

        whenever(dataSource.getShops()).thenReturn(Single.error(response))

        shopsRepositoryImpl.getShops()
            .test()
            .assertError(response)

        verify(dataSource).getShops()
    }
}