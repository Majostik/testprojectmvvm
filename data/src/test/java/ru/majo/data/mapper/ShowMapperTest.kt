package ru.majo.data.mapper

import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.majo.data.factory.ShopFactory
import ru.majo.data.model.ShopEntity
import ru.majo.domain.entities.Shop

@RunWith(MockitoJUnitRunner::class)
class ShowMapperTest {

    private val shopMapper = ShopMapper()

    @Test
    fun testMapToEntity() {
        val shop = ShopFactory.createShop()
        val shopEntity = shopMapper.mapToEntity(shop)

        assertModelAndEntity(shop, shopEntity)
    }

    @Test
    fun testMapFromEntity() {
        val shopEntity = ShopFactory.createShopEntity()
        val shop = shopMapper.mapFromEntity(shopEntity)

        assertModelAndEntity(shop, shopEntity)
    }

    private fun assertModelAndEntity(shop: Shop, shopEntity: ShopEntity) {
        assertEquals(shop.id, shopEntity.id)
        assertEquals(shop.number, shopEntity.number)
        assertEquals(shop.name, shopEntity.name)
        assertEquals(shop.address, shopEntity.address)
        assertEquals(shop.yearEarnings, shopEntity.yearEarnings)
        assertEquals(shop.coordinates.latitude, shopEntity.coordinates.latitude)
        assertEquals(shop.coordinates.longitude, shopEntity.coordinates.longitude)
        assertEquals(shop.openDate, shopEntity.openDate)
        assertEquals(shop.status.type, shopEntity.status)
    }
}