package ru.majo

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import ru.handh.chitaigorod.di.component.DaggerApplicationComponent

class Application : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.factory().create(this)
    }
}
