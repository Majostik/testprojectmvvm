package ru.majo.testproject.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    protected infix fun <T> LiveData<T>.observe(block: (T) -> Unit) {
        observe(this@BaseActivity, Observer<T> { t -> block.invoke(t) })
    }

    protected inline fun <reified T : ViewModel> lazyViewModel(): Lazy<T> {
        return threadUnsafeLazy {
            ViewModelProviders.of(this, viewModelFactory).get(T::class.java)
        }
    }

    fun <T> threadUnsafeLazy(block: () -> T): Lazy<T> {
        return lazy(LazyThreadSafetyMode.NONE, block)
    }
}
