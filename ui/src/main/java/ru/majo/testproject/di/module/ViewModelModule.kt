package ru.majo.testproject.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.handh.chitaigorod.di.util.ViewModelKey
import ru.majo.presentation.ViewModelFactory
import ru.majo.presentation.shops.ShopsViewModel

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ShopsViewModel::class)
    abstract fun shopsViewModel(viewModel: ShopsViewModel): ViewModel
}
