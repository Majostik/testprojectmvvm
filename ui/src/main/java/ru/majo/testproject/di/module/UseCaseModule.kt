package ru.majo.testproject.di.module

import dagger.Module
import dagger.Provides
import ru.majo.domain.interactor.shops.GetShopsUseCase
import ru.majo.domain.repository.ShopsRepository

@Module(includes = [RepositoryModule::class])
open class UseCaseModule {
    @Provides
    fun getShopsUseCase(shopsRepository: ShopsRepository): GetShopsUseCase {
        return GetShopsUseCase(shopsRepository)
    }
}
