package ru.handh.chitaigorod.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton
import ru.majo.Application
import ru.majo.testproject.di.module.ActivityModule
import ru.majo.testproject.di.module.ApplicationModule
import ru.majo.testproject.di.module.RepositoryModule
import ru.majo.testproject.di.module.UseCaseModule

@Singleton
@Component(
        modules = [
            ApplicationModule::class,
            AndroidSupportInjectionModule::class,
            ActivityModule::class,
            RepositoryModule::class,
            UseCaseModule::class]
)
interface ApplicationComponent : AndroidInjector<Application> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Application): ApplicationComponent
    }
}
