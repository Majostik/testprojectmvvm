package ru.majo.testproject.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.majo.testproject.di.scope.PerActivity
import ru.majo.testproject.ui.shops.ShopsActivity

@Module(includes = [ViewModelModule::class])
abstract class ActivityModule {

    @ContributesAndroidInjector
    @PerActivity
    abstract fun shopsActivity(): ShopsActivity
}
