package ru.majo.testproject.di.module

import dagger.Binds
import dagger.Module
import ru.majo.data.repository.ShopsRepositoryImpl
import ru.majo.domain.repository.ShopsRepository

@Module
abstract class RepositoryModule {
    @Binds
    internal abstract fun shopsRepository(repository: ShopsRepositoryImpl): ShopsRepository
}
