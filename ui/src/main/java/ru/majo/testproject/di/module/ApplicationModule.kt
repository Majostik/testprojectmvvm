package ru.majo.testproject.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import ru.majo.data.datasource.DataSource

@Module
open class ApplicationModule {

    @Provides
    fun provideApplication(app: ru.majo.Application): Application {
        return app
    }

    @Provides
    fun provideDataSource(): DataSource {
        return DataSource()
    }
}
