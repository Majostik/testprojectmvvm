package ru.majo.testproject.ui.shops

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_shops.*
import ru.majo.presentation.shops.ShopsViewModel
import ru.majo.testproject.R
import ru.majo.testproject.base.BaseActivity

class ShopsActivity : BaseActivity() {

    private val shopsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ShopsViewModel::class.java)
    }

    @Inject lateinit var shopsAdapter: ShopsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shops)
        onSetupLayout()
        onBindViewModel()
        shopsViewModel.getShops()
    }

    private fun onSetupLayout() {
        recyclerViewShops.apply {
            adapter = shopsAdapter
            layoutManager = LinearLayoutManager(this@ShopsActivity)
        }
    }

    private fun onBindViewModel() = with(shopsViewModel) {
        getShopsResultLiveData().observe { result ->
            result.doOnSuccess { shops ->
                shopsAdapter.items = shops.toMutableList()
                shopsAdapter.notifyDataSetChanged()
            }
        }
    }
}
