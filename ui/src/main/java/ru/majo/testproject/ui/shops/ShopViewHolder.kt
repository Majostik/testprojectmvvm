package ru.majo.testproject.ui.shops

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_shop.view.*
import ru.majo.domain.entities.Shop
import ru.majo.testproject.R

class ShopViewHolder(
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_shop, parent, false)
) {

    fun bind(shop: Shop) = with(itemView) {
        textViewName.text = shop.name
        textViewAddress.text = shop.address
    }
}
