package ru.majo.testproject.ui.shops

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import javax.inject.Inject
import ru.majo.domain.entities.Shop

class ShopsAdapter @Inject constructor() : RecyclerView.Adapter<ShopViewHolder>() {

    var items = mutableListOf<Shop>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopViewHolder =
        ShopViewHolder(parent)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) {
        holder.bind(items[position])
    }
}
