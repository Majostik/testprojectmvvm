package ru.majo.presentation.shops

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.majo.domain.entities.Shop
import ru.majo.domain.interactor.shops.GetShopsUseCase
import ru.majo.presentation.base.BaseViewModel
import ru.majo.presentation.base.LoadableResult
import javax.inject.Inject

public class ShopsViewModel @Inject public constructor(
    private val getShopsUseCase: GetShopsUseCase
) : BaseViewModel() {

    private val shopsResultLiveData = MutableLiveData<LoadableResult<List<Shop>>>()

    fun getShops() {
        getShopsUseCase.createSingle().execute(shopsResultLiveData)
    }

    fun getShopsResultLiveData(): LiveData<LoadableResult<List<Shop>>> {
        return shopsResultLiveData
    }
}