package ru.majo.presentation.base

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable

typealias Loading<T> = LoadableResult.Loading<T>
typealias Success<T> = LoadableResult.Success<T>
typealias Failure<T> = LoadableResult.Failure<T>

/**
 * Usage:
 * when (result) {
 *     is Loading -> { ... }
 *     is Success -> { value ->... }
 *     is Failure -> { throwable ->... }
 * }
 */
sealed class LoadableResult<R> {
    class Loading<R> : LoadableResult<R>()
    class Success<R>(val value: R) : LoadableResult<R>()
    class Failure<R>(val throwable: Throwable) : LoadableResult<R>() {
        val message = throwable.message
    }

    companion object {
        fun <R> loading(): LoadableResult<R> =
            Loading()
        fun <R> success(value: R): LoadableResult<R> =
            Success(value)
        fun <R> failure(throwable: Throwable): LoadableResult<R> =
            Failure(throwable)
    }

    val isLoading: Boolean get() = this is Loading

    val isSuccess: Boolean get() = this is Success

    val isFailure: Boolean get() = this is Failure

    /**
     * Usage:
     * val product: Product? = result.getOrNull()
     */
    fun getOrNull(): R? = when (this) {
        is Success -> value
        else -> null
    }

    /**
     * Usage:
     * val product: Product = result.getOrDefault {
     *     calculateDefaultValue()
     * }
     */
    fun getOrDefault(default: () -> R): R = when (this) {
        is Success -> value
        else -> default()
    }

    /**
     * Usage:
     * val product: Product = result.getOrDefault(Product.stub)
     */
    fun getOrDefault(defaultValue: R): R = when (this) {
        is Success -> value
        else -> defaultValue
    }
    /**
     * Usage:
     * result.doOnComplete {
     *     val value = it.getOrNull()
     *     handleValue(value)
     * }
     */
    inline fun doOnComplete(operation: (LoadableResult<R>) -> Unit) {
        when (this) {
            is Loading -> { /* Skip it */ }
            is Failure -> operation(
                failure(
                    throwable
                )
            )
            is Success -> operation(
                success(
                    value
                )
            )
        }
    }

    /**
     * Usage:
     * result.doOnLoading {
     *     showLoading()
     * }
     */
    inline fun doOnLoading(operation: () -> Unit) {
        when (this) {
            is Loading -> operation()
            is Failure -> { /* Skip it */ }
            is Success -> { /* Skip it */ }
        }
    }

    /**
     * Usage:
     * result.doOnSuccess {
     *     val value = it.getOrNull()
     *     handleValue(value)
     * }
     */
    inline fun doOnSuccess(operation: (R) -> Unit) {
        when (this) {
            is Loading -> { /* Skip it */ }
            is Failure -> { /* Skip it */ }
            is Success -> operation(value)
        }
    }

    /**
     * Usage:
     * result.doOnSuccess {
     *     val value = it.getOrNull()
     *     handleValue(value)
     * }
     */
    inline fun doOnFailure(operation: (Throwable) -> Unit) {
        when (this) {
            is Loading -> { /* Skip it */ }
            is Failure -> operation(throwable)
            is Success -> { /* Skip it */ }
        }
    }
}

fun <T> Single<T>.convert(block: (LoadableResult<T>) -> Unit): Disposable {
    return doOnSubscribe {
        block.invoke(LoadableResult.loading())
    }.subscribe({ value ->
        block.invoke(LoadableResult.success(value))
    }, { throwable ->
        block.invoke(LoadableResult.failure(throwable))
    })
}

fun <T> Flowable<T>.convert(block: (LoadableResult<T>) -> Unit): Disposable {
    return doOnSubscribe {
        block.invoke(Loading())
    }.subscribe(
        { value ->
            block.invoke(Success(value))
        }, { throwable ->
            block.invoke(Failure(throwable))
        })
}

fun <T> Observable<T>.convert(block: (LoadableResult<T>) -> Unit): Disposable {
    return doOnSubscribe {
        block.invoke(LoadableResult.loading())
    }.subscribe({ value ->
        block.invoke(LoadableResult.success(value))
    }, { throwable ->
        block.invoke(LoadableResult.failure(throwable))
    })
}

