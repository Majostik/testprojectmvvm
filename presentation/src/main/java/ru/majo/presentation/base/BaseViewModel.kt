package ru.majo.presentation.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ru.majo.presentation.utils.async

abstract class BaseViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    protected fun <T> Single<T>.execute(resultLiveData: MutableLiveData<ru.majo.presentation.base.LoadableResult<T>>): Disposable {
        return async()
                .convert {
                    resultLiveData.postValue(it)
                }
                .autoDispose()
    }

    protected fun <T> Single<T>.execute(block: (ru.majo.presentation.base.LoadableResult<T>) -> Unit): Disposable {
        return async()
                .convert {
                    block(it)
                }
                .autoDispose()
    }

    protected fun <T> Flowable<T>.execute(resultLiveData: MutableLiveData<ru.majo.presentation.base.LoadableResult<T>>): Disposable {
        return async()
                .convert {
                    resultLiveData.postValue(it)
                }
                .autoDispose()
    }

    protected fun <T> Flowable<T>.execute(block: (ru.majo.presentation.base.LoadableResult<T>) -> Unit): Disposable {
        return async()
            .convert {
                block.invoke(it)
            }
            .autoDispose()
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    protected fun Disposable.autoDispose(): Disposable {
        compositeDisposable.add(this)
        return this
    }
}
