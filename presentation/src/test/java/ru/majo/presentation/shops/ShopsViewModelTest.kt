package ru.majo.presentation.shops

import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.majo.domain.entities.Shop
import ru.majo.domain.interactor.shops.GetShopsUseCase
import ru.majo.presentation.base.LoadableResult
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.KArgumentCaptor
import com.nhaarman.mockitokotlin2.reset
import com.nhaarman.mockitokotlin2.times
import io.reactivex.subscribers.DisposableSubscriber
import org.junit.Before
import org.junit.Rule
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import io.reactivex.schedulers.Schedulers
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import junit.framework.Assert.assertEquals
import org.junit.After


@RunWith(MockitoJUnitRunner::class)
class ShopsViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val getShopsUseCase: GetShopsUseCase = mock()
    private val shops: List<Shop> = mock()

    private val shopsViewModel = ShopsViewModel(getShopsUseCase)

    private val observer: Observer<LoadableResult<List<Shop>>> = mock()

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        reset(getShopsUseCase, observer)
    }

    @Test
    fun testGetShopsSuccess() {
        whenever(getShopsUseCase.createSingle()).thenReturn(Single.just(shops))

        shopsViewModel.getShopsResultLiveData().observeForever(observer)

        shopsViewModel.getShops()

        verify(getShopsUseCase).createSingle()

        val argumentCaptor = ArgumentCaptor.forClass(LoadableResult::class.java)
        argumentCaptor.run {
            verify(observer, times(2)).onChanged(argumentCaptor.capture() as LoadableResult<List<Shop>>?)
            val (loadingResult, successResult) = allValues
            assertEquals(loadingResult.isLoading, true)
            assertEquals(successResult.isSuccess, true)
            assertEquals(successResult.getOrNull(), shops)
        }
    }

    @Test
    fun testGetShopsError() {
        val response = Throwable("Error response")

        whenever(getShopsUseCase.createSingle()).thenReturn(Single.error(response))

        shopsViewModel.getShopsResultLiveData().observeForever(observer)

        shopsViewModel.getShops()

        verify(getShopsUseCase).createSingle()

        val argumentCaptor = ArgumentCaptor.forClass(LoadableResult::class.java)
        argumentCaptor.run {
            verify(observer, times(2)).onChanged(argumentCaptor.capture() as LoadableResult<List<Shop>>?)
            val (loadingResult, errorResult) = allValues
            assertEquals(loadingResult.isLoading, true)
            assertEquals(errorResult.isFailure, true)
        }
    }


    @After
    fun tearDown() {
        RxAndroidPlugins.reset();
    }
}