package ru.majo.domain.entities

data class Coordinates(
    val latitude: Double,
    val longitude: Double
)