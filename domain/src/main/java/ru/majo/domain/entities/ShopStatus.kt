package ru.majo.domain.entities

enum class ShopStatus(val type: String) {
    OPEN("open"),
    SOON("soon"),
    CLOSED("closed"),
    REPAIR("repair")
}