package ru.majo.domain.entities

import java.util.Date

data class Shop(
    val id: Int,
    val number: Int,
    val name: String,
    val address: String,
    val yearEarnings: Int,
    val coordinates: Coordinates,
    val openDate: Date?,
    val status: ShopStatus
) {
    companion object {
        fun convertStatus(status: String): ShopStatus {
            return ShopStatus.CLOSED
        }
    }
}