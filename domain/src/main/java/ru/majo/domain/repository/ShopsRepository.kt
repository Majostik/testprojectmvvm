package ru.majo.domain.repository

import io.reactivex.Single
import ru.majo.domain.entities.Shop

interface ShopsRepository {
    fun getShops(): Single<List<Shop>>
    fun getShopById(id: Int): Single<Shop>
}