package ru.majo.domain.interactor.shops

import io.reactivex.Single
import ru.majo.domain.entities.Shop
import ru.majo.domain.interactor.SingleUseCase
import ru.majo.domain.repository.ShopsRepository

class GetShopsUseCase(
    private val shopsRepository: ShopsRepository
) : SingleUseCase<Nothing, List<Shop>> (){

    override fun createSingle(params: Nothing?): Single<List<Shop>> {
        return shopsRepository.getShops()
    }
}