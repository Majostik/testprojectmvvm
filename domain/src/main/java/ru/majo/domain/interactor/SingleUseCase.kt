package ru.majo.domain.interactor

import io.reactivex.Single
abstract class SingleUseCase<Params, T> {

    var params: Params? = null

    abstract fun createSingle(
        params: Params? = null
    ): Single<T>

}