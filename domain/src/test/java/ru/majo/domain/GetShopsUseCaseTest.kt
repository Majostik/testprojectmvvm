package ru.majo.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.majo.domain.entities.Shop
import ru.majo.domain.interactor.shops.GetShopsUseCase
import ru.majo.domain.repository.ShopsRepository

@RunWith(MockitoJUnitRunner::class)
class GetShopsUseCaseTest {

    private val shopsRepository: ShopsRepository = mock()
    private val shopsEntitiesList: List<Shop> = mock()

    private val getShopsUseCase = GetShopsUseCase(shopsRepository)

    @Test
    fun testGetShopsComplete() {
        whenever(shopsRepository.getShops()).thenReturn(Single.just(emptyList()))

        getShopsUseCase.createSingle()
            .test()
            .assertComplete()
    }

    @Test
    fun testGetShopsError() {
        val response = Throwable("Error response")

        whenever(shopsRepository.getShops()).thenReturn(Single.error(response))

        getShopsUseCase.createSingle()
            .test()
            .assertError(response)

        verify(shopsRepository).getShops()
    }

    @Test
    fun testGetShopsResult() {
        whenever(shopsRepository.getShops()).thenReturn(Single.just(shopsEntitiesList))

        getShopsUseCase.createSingle()
            .test()
            .assertValue(shopsEntitiesList)

        verify(shopsRepository).getShops()
    }
}